#!/usr/bin/python3
import argparse
import solr_api as sa

def tenant_status(tenant):
  scl_num_docs = sa.get_num_docs('frmsolr008.scl.five9.com',tenant)
  atl_num_docs = sa.get_num_docs('frmsolr008.atl.five9.com',tenant)
  percent = -1

  if scl_num_docs == atl_num_docs:
    percent = 100
  if scl_num_docs > atl_num_docs:
    percent = (atl_num_docs / scl_num_docs)*100
  else:
    percent = (scl_num_docs / atl_num_docs)*100

  return f"{tenant}\t{scl_num_docs}\t{atl_num_docs}\t{round(percent, 2)}%"
    
if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Shows the percentage of documents synced between DCs.')
  parser.add_argument('dc', choices=['scl', 'atl', 'ldn', 'qa01pdc', 'qa01bdc', 'qa02pdc', 'qa02bdc', 'tag03pdc', 'tag03bdc', ], help='DC to check for replica info.')
  parser.add_argument('tenant_ids', action='store', type=str, nargs="+", help="list of tenant IDs to check (leave out the 'tenant_' part)")

  args = parser.parse_args()

  dc = vars(args)['dc']
  host = sa.get_solr_node_to_query(dc)

  tenants = [f"tenant_{tenant}" for tenant in vars(args)['tenant_ids']]

  print ('tenant','SCL','ATL','Percent')
  for tenant in tenants:
    print(tenant_status(tenant))
