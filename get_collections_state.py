#!/usr/bin/python3
import solr_api as sa
import yaml

# Prints the collections state across all hosts in DC.
# Shows how many replicas per solr node and their status:
# e.g. 
# frmsolr050.scl.five9.com:
#   active: 382
# frmsolr051.scl.five9.com:
#   active: 297
# frmsolr052.scl.five9.com:
#   active: 373

solr_collections = {'ATL':{'frmsolr001.atl.five9.com':{}},
                    'SCL':{'frmsolr001.scl.five9.com':{}},
                    'LDN':{'frmsolr001.ldn.five9.com': {}}}

def collections_state():
  for dc in solr_collections:
    print(f"Getting collection data for {dc}")
    state = sa.clusterstate(list(solr_collections[dc].keys())[0])
    for c in state.values():
      for s in c['shards'].values():
        for r in s['replicas'].values():
          node = r['node_name'].split(':')[0]
          if node not in solr_collections[dc]:
            solr_collections[dc][node] = {r['state']:0}
          else:
            if r['state'] not in solr_collections[dc][node]:
              solr_collections[dc][node][r['state']] = 0
            if 'leader' in r or 1:
              solr_collections[dc][node][r['state']] += 1 
                             
collections_state()
print(yaml.safe_dump(solr_collections, default_flow_style=False))




