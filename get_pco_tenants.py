#!/usr/bin/python3
from solr_api import clusterstate
import mysql.connector

cluster = clusterstate('frmsolr011.atl.five9.com')

# Need correct password
cnx = mysql.connector.connect(host='10.7.88.50',
                      user='report',
                      password='report59',
                      database='Five9App')

cursor = cnx.cursor()
pco = cursor.execute("SELECT id, name FROM domain WHERE flags&1<<27")

print("Collection\tShards\tReplicas\tDomain")

for (id, name) in cursor:
  collection = f"tenant_{id}"

  if collection in cluster:
    replicas = 0     
    shards = len(cluster[collection]['shards'])
    for shard in cluster[collection]['shards'].values():
      replicas += len(shard['replicas'])

    print(f"{collection}\t{shards}\t{replicas}\t{name.decode()}")

cursor.close()
cnx.close()