#!/usr/bin/python3
import solr_api as sa
import yaml
import json
import argparse

parser = argparse.ArgumentParser(description='Prints Solr collection information.')
parser.add_argument('dc', choices=['scl', 'atl', 'ldn', 'qa01pdc', 'qa01bdc', 'qa02pdc', 'qa02bdc', 'tag03pdc', 'tag03bdc', 'tag04pdc'], help='DC to check for replica info.')
parser.add_argument('tenant_ids', action='store', type=str, nargs="+", help="list of tenant IDs to check (leave out the 'tenant_part)")
parser.add_argument('--output', '-o', choices=['json','yaml'], default='yaml', help="output format (json|yaml), defaults to yaml")

args = parser.parse_args()



dc = vars(args)['dc']
host = sa.get_solr_node_to_query(dc)
tenants = [f"tenant_{tenant}" for tenant in vars(args)['tenant_ids']]
output_format = vars(args)['output']

rows = {}
for tenant in tenants:
  rows.update(sa.collection_state(host, tenant))

if rows:
  if output_format == 'yaml':
    #print(yaml.safe_dump(rows, default_flow_style = False))
    print(yaml.safe_dump(rows, default_flow_style = None, width = 250))
  elif output_format == 'json':
    print(json.dumps(rows))
else:
  print("No collections found")