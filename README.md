# SolrUtil

## Description
Selection of utility scripts to manage Solr collections on-prem. Future scripts planned:

- Verifying healthy collections (possibly integrate into LogicMonitor)
- Get status of a collection
- Delete/Add replicas
- Move replicas (i.e. Solr rebalancing)

