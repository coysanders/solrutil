#!/usr/bin/python3
import argparse
import json
import requests


def getSolrLBIP(url):
  match url:
    case 'bom':
      return '10.63.24.97' 
    case 'del':
      return '10.63.28.21' 
    case 'frk':
      return '10.63.20.101' 
    case 'ldn1':
      return '10.63.16.113' 
    case 'mtl1':
      return '10.48.200.124' 
    case 'mtl2':
      return '10.48.206.135' 
    case 'nld':
      return '10.63.12.128' 
    case 'qa':
      return '10.219.232.252'
    case 'qar2':
      return '10.223.10.244' 
      # Current issue with qa42 where frmsolr LB not responding, going to a node which can change.

def getCollectionsStatus(httpResp):

  collectionsStatus = {
    'active': 0,
    'recovering': 0,
    'degraded': 0,
    'downed_shard': 0,
  }

  collections = httpResp.json()['cluster']['collections']
  for collection in collections:
    for shard in collections[collection]['shards']:
      state = collections[collection]['shards'][shard]['state']
      collectionsStatus[state] += 1
  
  return collectionsStatus

   

def main():

  parser = argparse.ArgumentParser(description="Get the aggregate status of all Solr collections in a GCP environment running Solr 8")
  parser.add_argument("dc", choices=['bom', 'del', 'frk', 'ldn1', 'mtl1', 'mtl2', 'nld', 'qa', 'qar2'], help="Google region to check")
  args = parser.parse_args()
  
  dc = vars(args)['dc']
  solrIP = getSolrLBIP(dc)

  print(f"Checking Solr collections on {dc}")
  resp = requests.get(f"http://{solrIP}:8983/solr/admin/collections?action=CLUSTERSTATUS")
  #print(resp.json())
  collectionsStatus = getCollectionsStatus(resp)
  for status in collectionsStatus:
    print(f"{status}: {collectionsStatus[status]}")


if __name__ == '__main__':
  main()
