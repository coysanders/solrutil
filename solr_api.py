#!/usr/bin/python3
# Utility commands that can be reused for multiple scripts.

import json
import requests
import sys

"""
class Logger(object):
  def __init__(self, filename):
    self.terminal = sys.stdout
    self.log = open(filename, "w")

  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)

  def flush(self):
    # needed for Python3 compatability.
    pass
"""

# Returns a frmsolr host based on DC that will be used to run API calls off of
#
# Note: Any solr node in the cluster can be used to query, this just defaults to the first one but any can be used to make API requests.
def get_solr_node_to_query(dc):
  if dc in ['scl', 'atl', 'ldn']:
      return f"frmsolr001.{dc}.five9.com"
  else:
    match dc:
      case 'qa01pdc':
        return 'qafrmsolr011.scl.five9lab.com'
      case 'qa01bdc':
        return 'qafrmsolr031.atl.five9lab.com'
      case 'qa02pdc':
        return 'qafrmsolr111.scl.five9lab.com'
      case 'qa02bdc':
        return 'qafrmsolr131.atl.five9lab.com'
      case 'tag03pdc':
        return 'qafrmsolr611.scl.five9lab.com'
      case 'tag03bdc':
        return 'qafrmsolr631.atl.five9lab.com'
      case 'tag04pdc':
        return 'tagfrmsolr011.scl.five9lab.com'
      case _:
        return 'invalid_dc_listed'

# Returns a zookeeper host based on DC that will be used to run API calls off of
#
# Note: Any solr node in the cluster can be used to query, this just defaults to the first one but any can be used to make API requests.
def get_zookeeper_node_to_query(dc):
  if dc in ['scl', 'atl', 'ldn']:
      return f"frmzk001.{dc}.five9.com"
  else:
    match dc:
      case 'qa01pdc':
        return 'qasolrzk011.scl.five9lab.com'
      case 'qa01bdc':
        return 'qasolrzk031.atl.five9lab.com'
      case 'qa02pdc':
        return 'qasolrzk111.scl.five9lab.com'
      case 'qa02bdc':
        return 'qasolrzk131.atl.five9lab.com'
      case 'tag03pdc':
        return 'qasolrzk611.scl.five9lab.com'
      case 'tag03bdc':
        return 'qasolrzk631.atl.five9lab.com'
      case 'tag04pdc':
        return 'tagsolrzk011.scl.five9lab.com'
      case _:
        return 'invalid_dc_listed'


# API call to get system info on a node in JSON format
# originally called systeminfo
# http://frmsolr008.scl.five9.com:8983/solr/admin/info/system?wt=json
def get_system_info(host, port=8983):
    system_data = {}
    url  = f"http://{host}:{port}/solr/admin/info/system?wt=json"
    resp = requests.get(url=url)
    if resp.status_code == 200:
        system_data = json.loads(resp.text)
    return system_data


# http://frmsolr005.scl.five9.com:8983/solr/zookeeper?wt=json&detail=true&path=/overseer/queue
# http://qafrmsolr211.scl.five9lab.com:8983/solr/zookeeper?wt=json&detail=true&path=%2Fclusterstate.json&view=graph&start=0&rows=0
def zookeeper(host, path, port=8983, **kwargs):
    data = {}
    url = f"http://{host}:{port}/solr/zookeeper?wt=json&detail=true&path={path}&start=0&rows=0"
    for k,v in kwargs.items():
        if v:
            url += "&%s=%s" % (k,v)
    #print(url)
    resp = requests.get(url=url)
    if resp.status_code == 200:
        data = json.loads(resp.text)
    return data


# Remove as a duplicate function
def clusterstate(host):
    return json.loads(zookeeper(host, '/clusterstate.json', view='graph')['znode']['data'])

def get_all_collections(host):
  return json.loads(zookeeper(host, '/clusterstate.json', view='graph')['znode']['data'])
    

# Gets the number of documents under a collection
# http://frmsolr005.scl.five9.com:8983/solr/tenant_122204_shard1_replica2/admin/luke?wt=json&show=index&numTerms=0
def get_num_docs(host, collection, shard=None):

    # Get first replica (any will work)
    node = get_active_replicas(host, collection, shard)[0]
    core = get_replica_data_by_field(host,collection,'node_name',node,'core',shard)[0]
    data = {}
    if not (node and core):
        return 0
    url = "http://%s/solr/%s/admin/luke?wt=json&show=index&numTerms=0" % (node.split('_')[0],core)
    resp = requests.get(url=url)
    if resp.status_code == 200:
        data = json.loads(resp.text)
    return data['index']['numDocs']


# Gets the state of a collection
def collection_state(host, collection, fType='name'):
    """
        fType:
          - name - collection=collection_name
          - status - collection=status_name
        status_name:
          - healthy
          - downed_shard
          - recovering
          - degraded
    """
    return json.loads(zookeeper(host, '/clusterstate.json', view='graph', filterType=fType, filter=collection)['znode']['data'])


# Returns set of active replicas of a collection.
def get_active_replicas(host, collection, shard=None):
    return get_replica_data_by_field(host, collection, 'state', 'active', 'node_name', shard)
    

# Gets replica data from a host/collection selected by following:
# - field: node_name, state
# - status: name of the node, state of replica (active, down, etc)
# - replica_key: core name, node name
def get_replica_data_by_field(host, collection, field, status, replica_key, shard=None):
    ret = []
    state = collection_state(host, collection)
    shards = replicas = state[collection]['shards']
    for s, sd in shards.items():
        if shard == None or shard == s:
            replicas = sd['replicas']
            for r,rd in replicas.items():
                if field in rd and rd[field] == status:
                    if replica_key in rd:
                        ret.append(rd[replica_key])
    return ret
