#!/usr/bin/python3

import argparse
import solr_api as sa
import requests


"""
  Find all replicas that are down in each collection and create a dictionary.
   containing relevant information about each down replica.
"""
def find_down_replicas(collections):
  down_replicas = {}
  for collection_name, collection_value in collections.items():
    for shard_key, shard_value in collection_value['shards'].items():
      for replica_key, replica_value in shard_value['replicas'].items():
        if replica_value['state'] == 'down':
          down_replicas[collection_name] = {}
          down_replicas[collection_name]['base_url'] = replica_value['base_url']
          down_replicas[collection_name]['shard'] = shard_key
          down_replicas[collection_name]['replica'] = replica_key
  return down_replicas


"""
  We will use the leader as the source to rebuild the replica from.
"""
def find_leaders(down_replicas, collections):
  for tenant, dr_values in down_replicas.items():
    for replica in collections[tenant]['shards'][dr_values['shard']]['replicas'].values():
      if 'leader' in replica:
        down_replicas[tenant]['leader'] = replica['node_name']


def main():
  parser = argparse.ArgumentParser(description='Looks for degraded collections and rebuilds them')
  parser.add_argument('dc', choices=['scl', 'atl', 'ldn', 'qa01pdc', 'qa01bdc', 'qa02pdc', 'qa02bdc', 'tag03pdc', 'tag03bdc', 'tag04pdc'], help='DC to check for replica info.')
  args = parser.parse_args()
  dc = vars(args)['dc']

  solr_node = sa.get_solr_node_to_query(dc)
  collections = sa.get_all_collections(solr_node)

  down_replicas = find_down_replicas(collections)
  find_leaders(down_replicas, collections)

  # We will automate this after testing. For now, just print out curl commands to run manually..
  for tenant,values in down_replicas.items():
    print(f"{tenant}:")
    if 'leader' not in values:
      print(f"No leader found for {tenant}, skipping...\n")
      continue
    delete_url = ("curl '" +
                  values['base_url'] +
                  "/admin/collections?action=DELETEREPLICA" +
                  "&collection=" + tenant +
                  "&shard=" + values['shard'] +
                  "&replica=" + values['replica'] + "'")
    print(delete_url)

    leader = values['leader'].replace('8983_solr', '8983/solr')
    source = values['base_url'].replace('8983/solr', '8983_solr')
    source = source.replace('http://','')
    add_url = ("curl '" +
               "http://" + leader +
               "/admin/collections?action=ADDREPLICA" +
               "&collection=" + tenant +
               "&shard=" + values['shard'] +
               "&node=" + source +
               "&wt=json&indent=true'")
    print(add_url)
    print("\n")


if __name__ == '__main__':
  main()