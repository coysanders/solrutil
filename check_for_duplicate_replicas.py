#!/usr/bin/python3
import solr_api as sa
import argparse


def get_dup_replicas_in_shard(replicas):
    duplicates = []
    found_replicas = []
    for rep in replicas.values():
        if 'leader' not in rep:
            if rep['node_name'] in found_replicas:
                duplicates.append((rep['node_name'].split(':')[0],rep['core']))
        found_replicas.append(rep['node_name'])
    return duplicates


def get_duplicates():
    print("Checking for duplicate replicas")
    cluster_state = sa.clusterstate(host)
    for tenant in cluster_state.values():
        for shard in tenant['shards'].values():
            dup = get_dup_replicas_in_shard(shard['replicas'])
            if dup:
                print(dup)

parser = argparse.ArgumentParser(description='Checks ')
parser.add_argument('dc', choices=['scl', 'atl', 'ldn', 'qa01pdc', 'qa01bdc', 'qa02pdc', 'qa02bdc', 'tag03pdc', 'tag03bdc', 'tag04pdc'], help='DC to check for replica info.')
args = parser.parse_args()

host = sa.get_solr_node_to_query(vars(args)['dc'])
#host = 'qafrmsolr611.scl.five9lab.com'

get_duplicates()