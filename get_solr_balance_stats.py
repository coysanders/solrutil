#!/usr/bin/python3
from copy import deepcopy
from datetime import datetime
import argparse
import json
import mysql.connector
import paramiko
import pathlib
import re
import requests
import solr_api as sa
import sys

"""
Script used to get the following solr data in an environment:
 
- Number of Solr nodes
- Number of replicas on each node
- Total size of replicas on each node
- Overall disk usage on /var"
"""

"""
Checks if the command line argument solr nodes are present in the datacenter.

e.g. frmsolr100.scl.five9.com may be a valid name of a node that passed validate_nodes(),
  but this will check if such a node exists.

"""
def check_nodes_exist(argument_nodes, solr_nodes):
  for node in argument_nodes:
    if node not in solr_nodes:
      sys.exit(f"Node {node} to be rebalanced was not found. Please check name.")


"""
  Writes out solr node data found to a file. And exits out of the program.

  This will be used to obtain all the replica/collection data of a Solr cluster once, and run multiple rebalance plans on this newly created
  output file. The benefit is speed on the intermediate rebalances as the script will not have to check the replica data again.

  As of 2023, SCL/ATL our largest clusters take over an hour to populate all relevant data. 
  When rebalances are planned, generate a JSON file first, then work off the JSON file to make your rebalance plans.
"""
def generate_solr_json(solr_nodes, dc):
  time_string = datetime.now().strftime("%Y-%m-%d-%H-%M")
  filename = f"{time_string}-{dc}.json"

  path = pathlib.Path("./solr_data")
  path.mkdir(mode=0o777, parents=False, exist_ok=True)

  with open("./solr_data/" + filename, "w") as outfile:
    outfile.write(json.dumps(solr_nodes))
    sys.exit()



"""
Creates a dictionary of solr nodes found as well as providing the following:
- number of replicas
- number of leaders
- disk related information used to help determine rebalancing.
- a list of all the replica URLs
"""
def get_collections_data(solr_node):
  solr_nodes = {}

  collections = sa.get_all_collections(solr_node)
  for cluster in collections.values():
    for shard in cluster['shards'].values():
      for replica in shard['replicas'].values():
        node = replica['node_name'].split(':')[0]
        if node not in solr_nodes:
          solr_nodes[node] = {
            'replicas': 0,
            'leaders' : 0,
            'replica_size_cumul_bytes:': 0,
            'var_disk_percent': 0.0,
            'var_disk_size_kbytes': 0,
            'var_disk_usage_kbytes': 0,
            'var_disk_available_kbytes': 0,
            'replica_list': {}
          }
        solr_nodes[node]['replicas'] += 1
        if 'leader' in replica:
          solr_nodes[node]['leaders'] += 1
        if replica['core'] not in solr_nodes[node]['replica_list']:
          solr_nodes[node]['replica_list'][replica['core']] = {} # Will contain the base url (location) and the eventual size, thus using a dict
        solr_nodes[node]['replica_list'][replica['core']]['base_url'] = replica['base_url']

  return solr_nodes


"""
Returns a list of pco tenants obtained from maindb.
"""
def get_pco_tenants():
  cnx = mysql.connector.connect(host='10.7.88.50',
                                user='report',
                                password='report59',
                                database='Five9App')
  cursor = cnx.cursor()
  pco = cursor.execute("SELECT id, name FROM domain WHERE flags&1<<27")
  pco_tenants = []
  for (id, name) in cursor:
    pco_tenants.append(f"tenant_{id}")
  cursor.close()
  cnx.close()
  return pco_tenants


"""
Uses the JSON file that's created from a previous run of this script when using the --output-json option.
To use the JSON file instead of querying the Solr cluster you use the --use-json option and specify the location of the JSON file.

See comments on generate_solr_json function as to why and when this should be used as an option.
"""
def get_replica_data_from_file(json_file):
  with open(json_file) as f:
    data = f.read()
  return json.loads(data)


"""
Makes API requests to get the size of replicas of each solr node and store them in solr_nodes.
"""
def get_replica_data_from_solr_cluster(solr_nodes):

  for node in solr_nodes:
    print(f"DEBUG: Getting replica data for {node}")
    for replica in solr_nodes[node]['replica_list']:
      base_url = solr_nodes[node]['replica_list'][replica]['base_url']
      req = f"{base_url}/admin/cores?action=STATUS&core={replica}&indexInfo=true&wt=json"
      #print(f"Making request {req}")
      for attempt in range(5):
        try:
          res = requests.get(req)
        except Exception as e:
          print(f"An error occured with request {req}:", e)
          print(f"Retrying attempt {attempt+1}")
          continue
        else:
          break # We succeeded in making API request to solr
      else:
        print(f"Could not make request {req}, continuing on")

      #print(f"size: {res.json()['status'][replica]['index']['sizeInBytes']}")
      try:
        replica_size_in_bytes = res.json()['status'][replica]['index']['sizeInBytes']
      except KeyError:
        print(f"Key error found: {res.json()}")
        print(f"Node: {node}")
        print(f"Replica: {replica}")
        print(f"Base URL: {base_url}")
        print(f"Req: {req}")
      solr_nodes[node]['replica_list'][replica]['size_in_bytes'] = replica_size_in_bytes
      if 'replica_size_cumul_bytes' not in solr_nodes[node]:
        solr_nodes[node]['replica_size_cumul_bytes'] = 0
      solr_nodes[node]['replica_size_cumul_bytes'] += replica_size_in_bytes
      #print(f"size: {solr_nodes[node]['replica_list'][replica]['size_in_bytes']}")
  return solr_nodes


"""
Validates that the correct subset of arguments are specified. Unable to do this strictly through argparse
"""
def validate_args(args):
  if vars(args)['use_json'] and vars(args)['output_json']:
    sys.exit("Cannot both generate and use a JSON output file. Either generate the file via --output-json, or use it with --use-json. Run script with -h for more details.")
  elif vars(args)['output_json'] and vars(args)['src_nodes']:
    sys.exit("No need to specify a list of nodes to rebalance if an output file is being generated instead. Run script with -h for more details.")
  elif vars(args)['use_json'] and not vars(args)['src_nodes']:
    sys.exit("Must specify nodes to create migration plan. Run script with -h for more details.")


"""
Ensures the names of frmsolr nodes specified as command line arguents have the correct format based on their DC location:

e.g.  SCL node  : frmsolr001.scl.five9.com
      QA02 node : qafrmsolr111.scl.five9lab.com

Note: This does not check if the node exists.
"""
def validate_nodes(nodes, dc):
  for node in nodes:
    match dc:
      case 'scl':
        if not re.search(r"^frmsolr\d{3}.scl.five9.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'atl':
        if not re.search(r"^frmsolr\d{3}.atl.five9.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'ldn':
        if not re.search(r"^frmsolr\d{3}.ldn.five9.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'qa01pdc':
        if not re.search(r"^qafrmsolr0\d{2}.scl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'qa01bdc':
        if not re.search(r"^qafrmsolr0\d{2}.atl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'qa02pdc':
        if not re.search(r"^qafrmsolr1\d{2}.scl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'qa02bdc':
        if not re.search(r"^qafrmsolr1\d{2}.atl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'tag03pdc':
        if not re.search(r"^qafrmsolr6\d{2}.scl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'tag03bdc':
        if not re.search(r"^qafrmsolr6\d{2}.atl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case 'tag04pdc':
        if not re.search(r"^tagfrmsolr0\d{2}.scl.five9lab.com$", node):
          sys.exit(f"Node {node} is not a valid node. Please double check.")
      case _:
        sys.exit(f"Invalid DC {dc} listed. Please use an appropriate DC")


"""
---------------------------------------------------------------------------------------------------------------------
"""


"""
node - host to check disk usage for
solr_nodes - to populate the values
"""
def get_var_disk_usage_kbytes(node, solr_nodes, toor_key_path):
  #print(f"DEBUG: Getting /var disk data for {node}")
  #print(f"DEBUG: Using key {toor_key_path}")
  try:
    #TODO need to figure out how to make this work for everyone, where do we assume private key will be for everyone?
    # ~/.ssh/toor_rsa.lab , ~/.ssh/toor_rsa.prod ?
    private_key = paramiko.RSAKey(filename=toor_key_path) #Toor key works
    #private_key = paramiko.RSAKey(filename='/home/csanders/.ssh/toor_rsa.lab') #Toor key works
    
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # https://stackoverflow.com/questions/70565357/paramiko-authentication-fails-with-agreed-upon-rsa-sha2-512-pubkey-algorithm
    # This will not be an issue when qa solr is off CentOS 6.10
    #ssh.connect(node, username='csanders', pkey=private_key, disabled_algorithms=dict(pubkeys=["rsa-sha2-512", "rsa-sha2-256"]))
    ssh.connect(node, username='toor', pkey=private_key)
    #ssh.connect(node, username='csanders', pkey=private_key, disabled_algorithms=dict(pubkeys=["rsa-sha2-512"]))

    # Run the 'df' command to get disk usage information
    stdin, stdout, stderr = ssh.exec_command(f"df /var")
    output = stdout.read().decode()

    # Parsing the output to extract relevant information
    lines = output.split('\n')
    if len(lines) >= 2:
      header_parts = lines[0].split()
      data_parts = lines[1].split()
      if len(header_parts) >= 6 and len(data_parts) >= 6:
        #partition = data_parts[0] # Not needed, but keep around just in case
        size = data_parts[1] 
        used = data_parts[2]
        available = data_parts[3]
        #percentage_used = data_parts[4]
        solr_nodes[node]['var_disk_percent']  = float(data_parts[2])/float(data_parts[1])
        solr_nodes[node]['var_disk_size_kbytes']  = int(data_parts[1])*1024
        solr_nodes[node]['var_disk_usage_kbytes'] = int(data_parts[2])*1024
        solr_nodes[node]['var_disk_available_kbytes'] = int(data_parts[3])*1024

  except Exception as e:
    print("An authentication error occurred:", e)
  finally:
    ssh.close()
    return solr_nodes




"""
  solr_nodes - Dictionary of each solr node's data
  num_nodes_to_find - Returns a subset of solr_nodes of size 'num_nodes_to_find' that have the most available /var disk availability
"""
def get_lowest_disk_usage_nodes(solr_nodes, num_nodes_to_find=1):
  #for node in solr_nodes:
  #  print(f"{node}: {solr_nodes[node]['var_disk_percent']}")
  #sys.exit()
  sorted_solr_nodes_by_available_size = sorted(solr_nodes.items(), key=lambda x:x[1]['var_disk_percent'], reverse=False)
  #return dict((x,y) for x,y in sorted_solr_nodes_by_available_size[0:num_nodes_to_find]) This will return largest size, not percent
  return dict((x,y) for x,y in sorted_solr_nodes_by_available_size[0:num_nodes_to_find])


"""
  Goes through the list of replicas on src_nodes and checks if they are present on any of the nodes in dest_nodes.
  If not it will be moved. Will first look at largest replicas to move and proceed forward.

  solr_nodes - dictionary of dictionaries of all solr data
  src_nodes - array of nodes to rebalance by name
  target_nodes - subset of solr_nodes that are targets to rebalance to.
  disk_usage_deltas - dictionary listing target disk usage deltas for each host
  tenant_deltas - dictionary listing of target tenant deltas for each host
"""
def find_replicas_to_migrate(solr_nodes, src_nodes, dest_nodes, disk_usage_deltas, tenant_deltas, pco_tenants):
  #print(f"Nodes to rebalance {src_nodes}")
  #print(f"Destination nodes {dest_nodes}")

  """
  solr_nodes[node] = {
    'replicas': 0,
    'leaders' : 0,
    'replica_size_cumul_bytes:': 0,
    'var_disk_percent': 0.0,
    'var_disk_size_kbytes': 0,
    'var_disk_usage_kbytes': 0,
    'var_disk_available_kbytes': 0,
    'replica_list': {}
  }

  disk_usage_deltas = { "qafrmsolr613.scl.five9lab.com" : 4.07,
                        "qafrmsolr616.scl.five9lab.com" : -4.07
                      }
  tenant_deltas = { "qafrmsolr613.scl.five9.lab.com" : 30,
                    "qafrmsolr616.scl.five9.lab.com" : 30,
                  }
  
  solr_nodes[node]['replica_list'][replica]['size_in_bytes'] = replica_size_in_bytes
  """
  print(f"Starting disk_usage_deltas: {disk_usage_deltas}")

  """
    This will be the following strategy to get both size and number of tenants rebalanced:
      - In this example we need to move 400 collections from SolrNode A to SolrNode B
      - Assume there are enough small collections than are < 25MB
      - We will start by grabbing largest collections first to reduce size, but leave a buffer of (400-selected_collections)*25MB
      - Once we hit that buffer, then we start grabbing small collections.
      - This will give us a mix of a few large collections and several small ones to migrate.
  """
  """
    replicas_to_move = {
      "frmsolr001.scl": {
        count: 3,
        replicas: {
          'tenant_134095' : {shard: 'shard1', 'destination: 'frmsolr052.scl' },
          'tenant_134096' : {shard: 'shard1', 'destination: 'frmsolr052.scl' },
          'tenant_134097' : {shard: 'shard2', 'destination: 'frmsolr053.scl' },
        }
      },
      "frmsolr002.scl": {
        count: 1,
        replicas: {
          'tenant_131197' : {shard: 'shard2', 'destination: 'frmsolr054.scl' },
        }
      }
    }
  """

  replicas_to_move = {}
  for node in src_nodes:
    replicas_to_move[node] = {}
    replicas_to_move[node]['count'] = 0
    replicas_to_move[node]['replicas'] = {}
  for node in dest_nodes:
    replicas_to_move[node] = {}
    replicas_to_move[node]['count'] = 0
    replicas_to_move[node]['replicas'] = {}

  for node in src_nodes:
    sorted_replicas_by_size = sorted(solr_nodes[node]['replica_list'].items(), key=lambda x:x[1]['size_in_bytes'], reverse=True)
    sorted_replicas_by_size = dict((x,y) for x,y in sorted_replicas_by_size) # convert tuple to dictionary

    # Go through each replica starting with biggest first and see if the lowest_node has it.
    for replica_name, replica_data in sorted_replicas_by_size.items():

      tenant = re.sub('_shard\d\_replica\d', '', replica_name) #  tenant_100000000000261_shard1_replica1 -> tenant_100000000000261
      #print(f"Checking if tenant {tenant} is in pco_tenants:")
      if tenant in pco_tenants:
        #print(f"tenant {tenant} found in pco tenant list, skipping as an option to rebalance")
        continue

      if not re.search('replica', replica_name):
        continue; # There's only one replica? Leave it alone
      
      #print(f"Checking {replica_name} with size {replica_data['size_in_bytes']}")
      
      # First check if we meet the size requirement, leaving enough room for smaller replicas with approximate size ~5 MB
      #print(f"disk_usage_deltas[{node}]: {disk_usage_deltas[node]}")
      buffer = (tenant_deltas[node] - replicas_to_move[node]['count']) * 1024 * 1024 * 1
      #print(f"buffer space: {buffer}")
      #print(f"tenant_deltas[{node}]: {tenant_deltas[node]}")
      #print(f"replicas_to_move[{node}]['count']: {replicas_to_move[node]['count']}")

      if disk_usage_deltas[node] - buffer <= 0:
        break # if we do, stop sorting for this node and go to next


      for low_node in dest_nodes:
        #print(f"low node: disk_usage_deltas[{low_node}]: {disk_usage_deltas[low_node]}")
        buffer = (tenant_deltas[low_node] - replicas_to_move[low_node]['count']) * 1024 * 1024 * 1
        #print(f"low node: buffer space: {buffer}")
        #print(f"low node: tenant_deltas[{low_node}]: {tenant_deltas[low_node]}")
        #print(f"low node: replicas_to_move[{low_node}]['count']: {replicas_to_move[low_node]['count']}")
        # Check if dest_nodes already meets requirement for size:
        if disk_usage_deltas[low_node] + buffer >= 0:
          print("Skipping?")
          continue # We don't want any more large replicas to move on this low_node, so don't make it a candidate.

        for replica in dest_nodes[low_node]['replica_list']:
          if not re.search(tenant, replica):
            #print(f"-- disk_usage_deltas[{node}]: {disk_usage_deltas[node]}")
            #print(f"-- replica_data['size_in_bytes]: {replica_data['size_in_bytes']}")
            if disk_usage_deltas[node] - replica_data['size_in_bytes'] >= 0:
              print(f"{tenant} {round(replica_data['size_in_bytes']/1024/1024/1024,2)} GB not found in {low_node} and size appropriate")
              print(f"Migrating tenant {node} -> {low_node}: {round(replica_data['size_in_bytes']/1024/1024/1024,2)} GB ")
              replicas_to_move[node]['count'] += 1
              shard = re.sub('_replica\d','', re.sub('tenant_\d+_','', replica_name)) #  tenant_100000000000261_shard1_replica1 -> shard1
              #print(f"replica_name: {replica_name}")
              #print(f"Shard: {shard}")
              replicas_to_move[node]['replicas'][tenant] = {}
              replicas_to_move[node]['replicas'][tenant]['shard'] = shard
              replicas_to_move[node]['replicas'][tenant]['destination'] = low_node
              disk_usage_deltas[node] -= replica_data['size_in_bytes']
              replicas_to_move[low_node]['count'] += 1
              replicas_to_move[low_node]['replicas'][tenant] = '' # We're not moving it, so just keeping track of what tenants are going to this host to avoid duplicates.
              disk_usage_deltas[low_node] += replica_data['size_in_bytes']
              print(f"Disk usage delta update: {disk_usage_deltas}")
            break # Don't add collection to another low node

    
    # Now here go through starting at those 25MB and below and move now that we have size covered
    for replica_name, replica_data in sorted_replicas_by_size.items():
      tenant = re.sub('_shard\d\_replica\d', '', replica_name) #  tenant_100000000000261_shard1_replica1 -> tenant_100000000000261
      #print(f"Checking if tenant {tenant} is in pco_tenants:")
      if tenant in pco_tenants:
        #print(f"tenant {tenant} found in pco tenant list, skipping as an option to rebalance")
        continue

      if not re.search('replica', replica_name):
        continue; # There's only one replica? Leave it alone

      # Skip down to the smallest replicas, probably faster to do it this way than to reorder? Maybe iterate backwards? #TODO
      #if replica_data['size_in_bytes'] > 26214400: #25 MB
      #if replica_data['size_in_bytes'] > 5242880: #5 MB
      if replica_data['size_in_bytes'] > 1048576: #1 MB
        continue
      if tenant_deltas[node] == replicas_to_move[node]['count']:
        break
      # We still need to find more tenants to move
      for low_node in dest_nodes:
        if replicas_to_move[low_node]['count'] == tenant_deltas[low_node]:
          continue
        for replica in dest_nodes[low_node]['replica_list']:
          if not re.search(tenant, replica):
            print(f"{tenant} {round(replica_data['size_in_bytes']/1024/1024,2)} MB not found in {low_node}")
            print(f"Migrating tenant {node} -> {low_node}: {round(replica_data['size_in_bytes']/1024/1024,2)} MB ")
            replicas_to_move[node]['count'] += 1
            replicas_to_move[node]['replicas'][tenant] = {}
            shard = re.sub('_replica\d','', re.sub('tenant_\d+_','', replica_name)) #  tenant_100000000000261_shard1_replica1 -> shard1
            #print(f"replica_name: {replica_name}")
            #print(f"Shard2: {shard}")
            replicas_to_move[node]['replicas'][tenant]['shard'] = shard
            replicas_to_move[node]['replicas'][tenant]['destination'] = low_node
            disk_usage_deltas[node] -= replica_data['size_in_bytes']
            replicas_to_move[low_node]['count'] += 1
            replicas_to_move[low_node]['replicas'][tenant] = '' # We're not moving it, so just keeping track of what tenants are going to this host to avoid duplicates.
            disk_usage_deltas[low_node] += replica_data['size_in_bytes']
            break # Don't add collection to another low node

  return replicas_to_move
# End of find_replicas_to_migrate()


"""
('tenant_770300000000070_shard1_replica3', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 8152086859})
('tenant_4421_shard1_replica1', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 950404274})
('tenant_100000000000261_shard1_replica1', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 48643758})
('tenant_670300000003770_shard1_replica2', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 17679237})
('tenant_5119_shard1_replica1', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 16982808})
('tenant_3210_shard1_replica1', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 13809926})
('tenant_1099_shard1_replica3', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 10950284})
('tenant_1110_shard1_replica3', {'base_url': 'http://qafrmsolr613.scl.five9lab.com:8983/solr', 'size_in_bytes': 10918220})
"""

"""
First determines how much data needs to be moved (size) and number of tenants.
Then this function goes through candidate tenants to move and generate a list.
"""
def generate_rebalance_list(solr_nodes, src_nodes, dest_nodes, pco_tenants):
  """
  solr_nodes ->
            'replicas': 0,
            'leaders' : 0,
            'replica_size_cumul_bytes:': 0,
            'var_disk_percent': 0.0,
            'var_disk_size_kbytes': 0,
            'var_disk_usage_kbytes': 0,
            'var_disk_available_kbytes': 0,
            'replica_list': {}
  solr_nodes[node]['replica_list'][replica]['size_in_bytes'] = replica_size_in_bytes

  replica: tenant_1000000000000000072_shard1_replica2
  """

  total_disk_size = 0
  total_disk_usage = 0
  total_disk_free = 0
  total_tenants = 0

  for node in src_nodes:
    total_disk_size += solr_nodes[node]['var_disk_size_kbytes']
    total_disk_usage += solr_nodes[node]['var_disk_usage_kbytes']
    total_disk_free += solr_nodes[node]['var_disk_available_kbytes']
    total_tenants += len(solr_nodes[node]['replica_list'])

  for node in dest_nodes:
    total_disk_size += solr_nodes[node]['var_disk_size_kbytes']
    total_disk_usage += solr_nodes[node]['var_disk_usage_kbytes']
    total_disk_free += solr_nodes[node]['var_disk_available_kbytes']
    total_tenants += len(solr_nodes[node]['replica_list'])
  print("Cumulative totals of nodes to perform rebalance on")
  print(f"Total Disk size: {round(total_disk_size/1024/1024/1024,2)} GB")
  print(f"Total Disk usage: {round(total_disk_usage/1024/1024/1024,2)} GB")
  print(f"Total Disk free: {round(total_disk_free/1024/1024/1024,2)} GB")

  target_percent = total_disk_usage / total_disk_size
  target_tenant_count = round(total_tenants/(len(src_nodes)+len(dest_nodes)))

  print(f"Overall Disk usage target percent: {round(target_percent*100, 2)}%")
  print(f"Total tenants: {total_tenants}\n")
  print(f"Tenant target for each node: {target_tenant_count}")

  """
  disk_usage_deltas = { "qafrmsolr613.scl.five9lab.com" : 4.07,
                        "qafrmsolr616.scl.five9lab.com" : -4.07
                      }
  """
  disk_usage_deltas = {}
  tenant_deltas = {}

  print("Nodes to rebalance from:")
  for node in src_nodes:
    disk_usage_deltas[node] = solr_nodes[node]['var_disk_usage_kbytes'] - (solr_nodes[node]['var_disk_size_kbytes'] * target_percent)
    tenant_deltas[node] = len(solr_nodes[node]['replica_list']) - target_tenant_count
    print(f"Target disk usage for {node} is {round(disk_usage_deltas[node]/1024/1024/1024, 2)} GB")
    print(f"Target tenant delta {tenant_deltas[node]}")

  print("Nodes to rebalance to:")
  for node in dest_nodes:
    disk_usage_deltas[node] = solr_nodes[node]['var_disk_usage_kbytes'] - (solr_nodes[node]['var_disk_size_kbytes'] * target_percent)
    tenant_deltas[node] = target_tenant_count - len(solr_nodes[node]['replica_list'])
    print(f"Target disk usage for {node} is {round(disk_usage_deltas[node]/1024/1024/1024, 2)} GB")
    print(f"Target tenant delta {tenant_deltas[node]}")

  replicas_to_move = find_replicas_to_migrate(solr_nodes, src_nodes, dest_nodes, disk_usage_deltas, tenant_deltas, pco_tenants)
  for node in src_nodes:
    for tenant in replicas_to_move[node]['replicas']:
      shard = replicas_to_move[node]['replicas'][tenant]['shard']
      destination = replicas_to_move[node]['replicas'][tenant]['destination']
      print(f"{tenant},{shard},{node}:8983_solr,{destination}:8983_solr")


def generate_solr_rundown_file(solr_nodes):
  output_file = open("solr_balance_statistics.txt", "w")

  print("Summary:", file=output_file)
  print("=============================================================", file=output_file)

  for node in solr_nodes:
    #if node != 'qafrmsolr011.scl.five9lab.com':
    #  continue
    print(f"\tNode: {node}", file=output_file)
    print(f"\tReplicas: {solr_nodes[node]['replicas']}", file=output_file)
    print(f"\tLeaders: {solr_nodes[node]['leaders']}", file=output_file)
    print(f"\tReplica Size (cumul.): {round(solr_nodes[node]['replica_size_cumul_bytes']/1024/1024/1024, 2)} GB", file=output_file)
    print(f"\t/var disk size: {round(solr_nodes[node]['var_disk_size_kbytes']/1024/1024, 2)} GB", file=output_file)
    print(f"\t/var disk usage: {round(solr_nodes[node]['var_disk_usage_kbytes']/1024/1024, 2)} GB", file=output_file)
    print(f"\t/var disk available: {round(solr_nodes[node]['var_disk_available_kbytes']/1024/1024, 2)} GB", file=output_file)

  print("Replica rundown:", file=output_file)
  print("=============================================================", file=output_file)

  for node in solr_nodes:
    #if node != 'qafrmsolr011.scl.five9lab.com':
    #  continue
    print(f"\tNode: {node}", file=output_file)
    for replica in solr_nodes[node]['replica_list']:
      print(f"\t\t{replica} : {round(solr_nodes[node]['replica_list'][replica]['size_in_bytes']/1024/1024, 2)} MB", file=output_file)

  



def main():
  parser = argparse.ArgumentParser(description='Generates a Solr rebalance plan')
  parser.add_argument('dc', choices=['scl', 'atl', 'ldn', 'qa01pdc', 'qa01bdc', 'qa02pdc', 'qa02bdc', 'tag03pdc', 'tag03bdc', 'tag04pdc'], help='DC to check')
  parser.add_argument('--src-nodes', type=str, nargs="+", required=False, help="Nodes to migrate replicas off of")
  parser.add_argument('--dest-nodes', type=str, nargs="+", required=False, help="Nodes to migrate replicas to. If this argument is not specified, the script will find the best match based on disk usage.")
  parser.add_argument('--output-json', action="store_true", required=False, help="Specify whether to write current Solr data to a file instead of determining rebalancing. Useful when planning multiple rebalances and the Solr cluster hasn't changed.")
  parser.add_argument('--use-json', metavar="<json file>", type=str, nargs=1, required=False, help="Output file created using --output-json flag. This will genereate a rebalance plan using the file content instead of querying the cluster. System disk usage will still be queried.")

  args = parser.parse_args()
  validate_args(args)

  dc = vars(args)['dc']

  src_nodes = ''
  if vars(args)['src_nodes']:
    src_nodes = vars(args)['src_nodes']
    validate_nodes(src_nodes, dc)

  solr_node = sa.get_solr_node_to_query(dc)

  if vars(args)['use_json']:
    replica_data_file = vars(args)['use_json'][0]
    print(f"Using replica file {replica_data_file}")
    solr_nodes = get_replica_data_from_file(replica_data_file)
  else:
    print(f"Running check on {dc}")
    solr_nodes = get_collections_data(solr_node)
    check_nodes_exist(src_nodes, solr_nodes)
    solr_nodes = get_replica_data_from_solr_cluster(solr_nodes)

  if vars(args)['output_json']:
    print("Writing Replica data to file. Will not generate a rebalance strategy")
    generate_solr_json(solr_nodes, dc) # Program will exit after generating this file.

  pco_tenants = get_pco_tenants()

  toor_key_path = ''
  if dc in ['scl', 'atl', 'ldn']:
    toor_key_path = '/home/csanders/.ssh/toor_rsa.prod'
  else:
    toor_key_path = '/home/csanders/.ssh/toor_rsa.lab'

  dest_nodes = {}
  if vars(args)['dest_nodes']:
    dest_node_list = vars(args)['dest_nodes']
    validate_nodes(dest_node_list, dc)
    check_nodes_exist(dest_node_list, solr_nodes)
    print("Using command line specified nodes as targets:")
    for node in dest_node_list:
      print(node)
      #print(solr_nodes[node])
      dest_nodes[node] = solr_nodes[node]
    print("Getting solr node disk usage data...")
    for host in solr_nodes:
      if host not in dest_nodes.keys() and host not in src_nodes:
        continue
      print(f"Checking host {host}")
      solr_nodes = get_var_disk_usage_kbytes(host, solr_nodes, toor_key_path)
  else: 
    print("Getting solr node disk usage data...")
    for host in solr_nodes:
      solr_nodes = get_var_disk_usage_kbytes(host, solr_nodes, toor_key_path)

    dest_nodes = get_lowest_disk_usage_nodes(solr_nodes, len(src_nodes))
    print("Following nodes found with least used disk space that will be used as targets:")
    for node in dest_nodes:
      disk_gb_free = round(dest_nodes[node]['var_disk_available_kbytes']/1024/1024/1024, 2)
      num_tenants = len(dest_nodes[node]['replica_list'])
      print(f"{node}: {disk_gb_free} GB free /var disk usage, {num_tenants} tenants")
  print()

  print("Following nodes that are to be rebalanced:")
  for node in src_nodes:
    disk_gb_free = round(solr_nodes[node]['var_disk_available_kbytes']/1024/1024/1024, 2)
    num_tenants = len(solr_nodes[node]['replica_list'])
    print(f"{node}: {disk_gb_free} GB free /var disk usage, {num_tenants} tenants")
  print()


  generate_rebalance_list(solr_nodes, src_nodes, dest_nodes, pco_tenants)

  #sys.exit()
  generate_solr_rundown_file(solr_nodes)

if __name__ == '__main__':
  main()