#!/usr/bin/python3

import argparse
import json
from kazoo.client import KazooClient
import solr_api as sa

parser = argparse.ArgumentParser(description='Generates deletion URLs for all Solr collections in an environment')
parser.add_argument('dc', choices=['scl', 'atl', 'ldn', 'qa01pdc', 'qa01bdc', 'qa02pdc', 'qa02bdc', 'tag03pdc', 'tag03bdc', 'tag04pdc'], help='DC to check for replica info.')

args = parser.parse_args()

dc = vars(args)['dc']
host = sa.get_zookeeper_node_to_query(dc)

zk = KazooClient(hosts=f"{host}:2181")
zk.start()

collections = zk.get_children("/collections")
for c in collections:
  if not zk.exists("/collections/%s/leaders/shard1" % c):
    continue
  state = zk.get("/collections/%s/leaders/shard1" % c)
  l = json.loads(state[0])
  url = ("%s/admin/collections?action=DELETE&name=%s" % (l['base_url'], c))
  print(url) 
