#!/usr/bin/python3
# Originally solr_sysinfo.py
from copy import deepcopy
from time import gmtime, strftime
import solr_api as sa
import xlsxwriter

solr_collections = {'SCL': {'frmsolr001.scl.five9.com': 0},
                    'ATL': {'frmsolr001.atl.five9.com': 0},
                    'LDN': {'frmsolr001.ldn.five9.com': 0}}

solr_leaders = deepcopy(solr_collections)

def get_collections_counts():
    dc_list_test = {
        'LDN': 'frmsolr001.ldn.five9.com', 
    }
    dc_list = {
        'SCL': 'frmsolr001.scl.five9.com', 
        'ATL': 'frmsolr001.atl.five9.com', 
        'LDN': 'frmsolr001.ldn.five9.com', 
    }

    for dc,solr_node in dc_list.items():
        #print(f"DC: {dc}")
        #print(f"solr_node: {solr_node}")
        #print(solr_collections)
        state = sa.clusterstate(solr_node)
        for clusters in state.values():
            for shards in clusters['shards'].values():
                for replicas in shards['replicas'].values():
                    node = replicas['node_name'].split(':')[0]
                    if node not in solr_collections[dc]:
                        solr_collections[dc][node] = 0
                    solr_collections[dc][node] += 1
                    if 'leader' in replicas:
                        if node not in solr_leaders[dc]:
                            solr_leaders[dc][node] = 0
                        solr_leaders[dc][node] += 1


def createWorkbook():
    timestamp = strftime("%Y-%m-%d_%H-%M", gmtime())
    wb = xlsxwriter.Workbook(f'solr_stat-{timestamp}.xlsx')
    ws = wb.add_worksheet()
    row = 1
    ws.write(f"A{row}", 'DC')
    ws.write(f"B{row}", 'Solr host')
    ws.write(f"C{row}", '# Replicas')
    ws.write(f"D{row}", '# Leaders')
    ws.write(f"E{row}", 'Load Avg.')
    ws.write(f"F{row}", 'Total Swap')
    ws.write(f"G{row}", 'Swap %')
    ws.write(f"H{row}", 'File descriptors %')
    ws.write(f"I{row}", 'JVM %')

    percent_format = wb.add_format({'num_format': '0.00%'})
    ws.set_column('G:H', None, percent_format)

    return wb, ws, row


def main():
    wb, ws, row = createWorkbook()
    get_collections_counts()

    for dc, hosts in solr_collections.items():
        for solr_node, replicas in hosts.items():
            row += 1
            system_info = sa.get_system_info(solr_node)
            
            ws.write(f"A{row}", dc)
            ws.write(f"B{row}", solr_node)
            ws.write(f"C{row}", replicas)

            num_leaders = 0
            if solr_node in solr_leaders[dc]:
                num_leaders = solr_leaders[dc][solr_node]
            ws.write(f"D{row}", num_leaders)

            ws.write(f"E{row}", system_info['system']['systemLoadAverage'])

            total_swap = system_info['system']['totalSwapSpaceSize'] / 1024 / 1024
            ws.write(f"F{row}", f"{round(total_swap)}M")

            swap_used = total_swap - system_info['system']['freeSwapSpaceSize'] / 1024 /1024

            #ws.write(f"G{row}", f"{swap_used/total_swap*100:4.2f}%")
            ws.write(f"G{row}", swap_used/total_swap)

            ofdc = system_info['system']['openFileDescriptorCount']
            mfdc = system_info['system']['maxFileDescriptorCount']
            #ws.write(f"H{row}", f"{ofdc/mfdc*100:4.2f}%")
            ws.write(f"H{row}", ofdc/mfdc)

            ws.write(f"I{row}", system_info['jvm']['memory']['used'])

            ws.autofilter("A1:I1") # +1 for header column

    wb.close()

if __name__ == '__main__':
    main()